import { Route, Routes } from "react-router-dom";
import MainLayout from "./layouts/MainLayout.tsx";
import ArticlesList from "./pages/ArticlesList.tsx";
import AddArticle from "./pages/AddArticle.tsx";
import { ArticlesService } from "./core/articles/services/ArticlesService.ts";
import { createContext } from "react";

interface ArticleServiceContextType {
  articleService: ArticlesService;
}

const articleContextValue: ArticleServiceContextType = {
  articleService: new ArticlesService(),
};

export const ArticleServiceContext =
  createContext<ArticleServiceContextType>(articleContextValue);

function App() {
  return (
    <ArticleServiceContext.Provider value={articleContextValue}>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route index element={<ArticlesList />} />
          <Route path="add-article" element={<AddArticle />} />
        </Route>
      </Routes>
    </ArticleServiceContext.Provider>
  );
}

export default App;
