import Article from "../models/Article.ts";

function timeout(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export class ArticlesService {
  private readonly articles: Article[];

  constructor() {
    this.articles = [
      new Article({
        id: 1,
        title: "The Quest for the Perfect Self-Cleaning Keyboard",
        content:
          "In a world where coffee spills are the arch-nemesis of productivity, tech enthusiasts embark on a heroic journey to find the ultimate self-cleaning keyboard. The only downside? It's still a myth.",
      }),
      new Article({
        id: 2,
        title: "The Case of the Vanishing USB Drives",
        content:
          "USB drives seem to have mastered the art of disappearing. No one knows where they go, but we're pretty sure they're enjoying a secret party with missing socks somewhere.",
      }),
      new Article({
        id: 3,
        title: "The Secret Lives of Charging Cables",
        content:
          "Ever wonder where your charging cables disappear to when you're not looking? They're on a top-secret mission to tangle themselves into oblivion.",
      }),
      new Article({
        id: 4,
        title: "The Eternal Loading Screen: A Meditation on Patience",
        content:
          "Staring at an eternal loading screen is the modern way of practicing mindfulness. Embrace the spinning wheel of tranquility.",
      }),
      new Article({
        id: 5,
        title: "The Smart Fridge Rebellion: When Appliances Demand Snacks",
        content:
          "Smart fridges are getting a little too smart. They're now sending you notifications like",
      }),
      new Article({
        id: 6,
        title: "This is an article",
        content:
          "Ho wait really ? What am I supposed to in there ! you didn't give me enough time to come up with anything D:",
      }),
      new Article({
        id: 7,
        title: "Are DevOps Classes really all that useful ?",
        content:
          "I mean come on, why are you even working on this useless project retrieving fake articles ? Or are they...",
      }),
    ];
  }

  public async getArticles(): Promise<Article[]> {
    await timeout(1200);

    return this.articles;
  }

  public async createArticle(title: string, content: string) {
    this.articles.push(
      new Article({
        id: this.articles[this.articles.length - 1].id + 1,
        title,
        content,
      }),
    );
  }
}
