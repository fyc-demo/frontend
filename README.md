# Example React Project

## Requirements

- node.js

## setup

### Install dependencies

```sh
npm install
```

## Run the project in dev mode

```sh
npm run dev
```

## Build the project in production mode

```sh
npm run build
```
